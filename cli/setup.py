from sys import version_info
from setuptools import find_packages, setup
from os.path import join, dirname

if version_info[:2] < (3, 5):
    raise RuntimeError(
        'Unsupported python version %s.' % '.'.join(version_info)
    )

_NAME = 'app'
setup(
    name=_NAME,
    version='1.0',
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README')).read(),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    author='TeSS_Lime',
    include_package_data=True,
    install_requires=[
        'cryptography==2.8'
    ],
    entry_points={ 'console_scripts': [f'{_NAME} = {_NAME}.main:main' ] }
)