#!/bin/env python3

import logging

"""
Definition of logging
"""
logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
 
def main(): 
   logging.info('Test')
   
if __name__ == '__main__': 
    main()