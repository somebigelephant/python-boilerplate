FROM python:3.9

ADD py-app /app

RUN pip3 install --upgrade pip
RUN pip3 install /app

ENTRYPOINT [ "app" ]
